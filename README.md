提供微服务的注册与发现
第一版：
将该微服务部署在了 docker，通过localhost:8761 或者 localhost:8762 都可访问，两个端口相互注册。
过程：项目打包，使用 docker build -t isclab/eureka-discovery . 打包镜像；
使用 docker-compose up -d 启动。会有两个容器被启动。
